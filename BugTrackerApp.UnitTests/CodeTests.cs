﻿using System;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BugTrackerApp.UnitTests
{
    [TestClass]
    public class CodeTests : Code
    {
        /// <summary>
        ///     Check that variables return correct values as strings.   
        ///</summary>
        [TestMethod]
        public void Variable_Returns_Correct_Value_As_String()
        {
            string key = "key";
            int value = 10;
            var code = new Code();

            // Act
            code.variables.Add(key, value);
            var result = code.getVariable(key);

            //Assert
            Assert.AreEqual(value.ToString(), result);
        }

        /// <summary>
        ///     Check that variables return correct values as integers.   
        ///</summary>
        [TestMethod]
        public void Variable_Returns_Correct_Value_As_Integer()
        {
            string key = "key";
            int value = 10;
            var code = new Code();

            // Act
            code.variables.Add(key, value);
            var result = code.getVariableAsInt(key);

            //Assert
            Assert.AreEqual(value, result);
        }

        /// <summary>
        ///     Check that variable return false if undefined.   
        ///</summary>
        [TestMethod]
        public void Variable_Returns_False_If_Undefined()
        {
            string key = "key";
            var code = new Code();

            // Act
            var result = code.variableExists(key);

            //Assert
            Assert.IsFalse(result);
        }

        /// <summary>
        ///     Check that variable return true if defined.   
        ///</summary>
        [TestMethod]
        public void Variable_Returns_True_If_Defined()
        {
            string key = "key";
            int value = 10;
            var code = new Code();

            // Act
            code.variables.Add(key, value);
            var result = code.variableExists(key);

            //Assert
            Assert.IsTrue(result);
        }
    }
}

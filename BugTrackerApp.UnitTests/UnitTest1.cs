﻿using System;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BugTrackerApp.UnitTests
{
    [TestClass]
    public class CommandTests
    {
        /// <summary>
        ///     Check that the circle command returns true using valid parameters.
        /// </summary>
        [TestMethod]
        public void ParseSize_For_Circle_Should_Return_True()
        {
            // Arrange
            Command command = new Command();
            Form1 form = new Form1();

            // Act
            var result = command.parseSize(form, "circle", "500");

            //Assert
            Assert.IsTrue(result);
        }

        /// <summary>
        ///     Check that the circle command returns false using invalid parameters.
        /// </summary>
        [TestMethod]
        public void ParseSize_For_Circle_Should_Return_False()
        {
            // Arrange
            Command command = new Command();
            Form1 form = new Form1();

            // Act
            var result = command.parseSize(form, "circle", "qwerty");

            //Assert
            Assert.IsFalse(result);
        }

        /// <summary>
        ///     Check that the rectangle command returns true using valid parameters.
        /// </summary>
        [TestMethod]
        public void ParseSize_For_Rectangle_Should_Return_True()
        {
            // Arrange
            Command command = new Command();
            Form1 form = new Form1();

            // Act
            var result = command.parseSize(form, "rectangle", "100,100");

            //Assert
            Assert.IsTrue(result);
        }

        /// <summary>
        ///     Check that the rectangle command returns true using invalid parameters.
        /// </summary>
        [TestMethod]
        public void ParseSize_For_Rectangle_Should_Return_False()
        {
            // Arrange
            Command command = new Command();
            Form1 form = new Form1();

            // Act
            var result = command.parseSize(form, "rectangle", "A,B");

            //Assert
            Assert.IsFalse(result);
        }

        /// <summary>
        ///     Check that the triangle command returns true using valid parameters.
        /// </summary>
        [TestMethod]
        public void ParseSize_For_Triangle_Should_Return_True()
        {
            // Arrange
            Command command = new Command();
            Form1 form = new Form1();

            // Act
            var result = command.parseSize(form, "triangle", "100,100,100");

            //Assert
            Assert.IsTrue(result);
        }

        /// <summary>
        ///     Check that the triangle command returns true using invalid parameters.
        /// </summary>
        [TestMethod]
        public void ParseSize_For_Triangle_Should_Return_False()
        {
            // Arrange
            Command command = new Command();
            Form1 form = new Form1();

            // Act
            var result = command.parseSize(form, "triangle", "A,B,C");

            //Assert
            Assert.IsFalse(result);
        }
        /// <summary>
        ///     Check that the displayMessage function delivers a message using messageBox.
        /// </summary>
        [TestMethod]
        public void DisplayMessage_returnsTrue()
        {
            // Arrange
            Command command = new Command();

            // Act
            var result = command.displayMessage("This too shall pass.");

            //Assert
            Assert.IsTrue(result);
        }

        /// <summary>
        ///     Check that the drawTo function draws a line.
        /// </summary>
        [TestMethod]
        public void DrawTo_returnsTrue()
        {
            // Arrange
            Form1 form = new Form1();

            // Act
            var result = form.drawTo();

            //Assert
            Assert.IsTrue(result);
        }

        /// <summary>
        ///     Check that the drawLine function draws a line.
        /// </summary>
        [TestMethod]
        public void DrawLine_returnsTrue()
        {
            // Arrange
            Form1 form = new Form1();

            // Act
            var result = form.drawLine();

            //Assert
            Assert.IsTrue(result);
        }

        /// <summary>
        ///     Check that the drawCircle function draws a circle when passed a radius.
        /// </summary>
        [TestMethod]
        public void DrawCircle_returnsTrue()
        {
            // Arrange
            Form1 form = new Form1();

            // Act
            var result = form.drawCircle(100);

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        ///     Check that the drawRectangle draws a rectanlge when passed parameters.
        /// </summary>
        [TestMethod]
        public void DrawRectangle_returnsTrue()
        {
            // Arrange
            Form1 form = new Form1();

            // Act
            var result = form.drawRectangle(200,100);

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        ///     Check that the drawTriangle function draws a triangle when passed parameters.
        /// </summary>
        [TestMethod]
        public void DrawTriangle_returnsTrue()
        {
            // Arrange
            Form1 form = new Form1();

            // Act
            var result = form.drawTriangle(100,100,100);

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        ///     Check that the clearOutput function clears the PictureBox.
        /// </summary>
        [TestMethod]
        public void Clear_OutputBox_returnsTrue()
        {
            // Arrange
            Form1 form = new Form1();

            // Act
            var result = form.clear();

            //Assert
            Assert.IsTrue(result);
        }
    }
}

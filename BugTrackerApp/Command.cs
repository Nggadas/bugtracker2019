﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackerApp
{
    public class Command : Form1
    {
        public void delegateCommand(Form1 form, string commandText, string[] inputText)
        {
            if (!inputText.Equals("") && commandText.Trim().ToLower().Equals("run"))
            {
                Code code = new Code();
                code.parseProgram(form, inputText);
            } 
            else
            {
                if (commandText.Equals("run"))
                {
                    DrawAllShapes(form);
                }
                else if (commandText.Equals("line"))
                {
                    drawLine(form);
                }
                else if (commandText.Equals("circle"))
                {
                    drawCircle(form);
                }
                else if (commandText.Equals("rectangle"))
                {
                    drawRectangle(form);
                }
                else if (commandText.Equals("triangle"))
                {
                    drawTriangle(form);
                }
                else if (commandText.Equals("clear"))
                {
                    form.clear();
                }
                else if (commandText.Equals("reset"))
                {
                    if (reset(form))
                    {
                        displayMessage("Pen position has been reset.");
                    }
                }
                else if (commandText.Equals("exit") || commandText.Equals("quit"))
                {
                    exit();
                }
                else
                {
                    if (commandText.Equals(""))
                    {
                        displayMessage("You need to type a command.");
                    }
                    else
                    {
                        string[] commandList = commandText.Trim().ToLower().Split(' ');
                        string text = commandList[0];

                        if (text.Equals("moveto") || text.Equals("drawto"))
                        {
                            if (commandList.Length.Equals(2))
                            {
                                string points = commandList[1];
                                parsePoints(form, text, points);
                            }
                            else
                            {
                                displayMessage("Command needs parameters, try typing <command> <x,y>.");
                            }
                        }
                        else if (text.Equals("circle") || text.Equals("rectangle") || text.Equals("triangle"))
                        {
                            if (commandList.Length.Equals(2))
                            {
                                form.useInputPoints = true;
                                string points = commandList[1];
                                parseSize(form, text, points);
                            }
                        }
                        else
                        {
                            displayMessage("Invalid command, try again.");
                        }
                    }
                }
            }
        }

        public void DrawAllShapes(Form1 form)
        {
            form.drawLine();
            form.drawRectangle(0, 0);
            form.drawTriangle(0, 0, 0);
            form.drawCircle(0);
        }

        public void drawLine(Form1 form)
        {
            form.useInputPoints = true;
            form.drawLine();
        }

        public void drawRectangle(Form1 form)
        {
            form.useInputPoints = true;
            form.drawRectangle(0,0);
        }

        public void drawCircle(Form1 form)
        {
            form.useInputPoints = true;
            form.drawCircle(0);
        }

        public void drawTriangle(Form1 form)
        {
            form.useInputPoints = true;
            form.drawTriangle(0,0,0);
        }

        public bool parseSize(Form1 form, string text, string size)
        {
            string[] pointsList = size.Split(',');

            if (text.Equals("circle"))
            {
                if (Int32.TryParse(pointsList[0], out int radius))
                {
                    form.drawCircle(radius);
                    return true;
                }
                else
                {
                    displayMessage("Invalid parameters, try again.");
                    return false;
                }
            }
            else if (text.Equals("rectangle"))
            {
                if (Int32.TryParse(pointsList[0], out int width) && Int32.TryParse(pointsList[1], out int height))
                {
                    form.drawRectangle(width, height);
                    return true;
                }
                else
                {
                    displayMessage("Invalid parameters, try again.");
                    return false;
                }
            }
            else if (text.Equals("triangle"))
            {
                if (Int32.TryParse(pointsList[0], out int bse) && Int32.TryParse(pointsList[1], out int adj) && Int32.TryParse(pointsList[1], out int hyp))
                {
                    form.drawTriangle(bse, adj, hyp);
                    return true;
                }
                else
                {
                    displayMessage("Invalid parameters, try again.");
                    return false;
                }
            }
            return false;
        }

        public bool parsePoints(Form1 form, string text, string points)
        {
            string[] pointsList = points.Split(',');

            if (text.Equals("moveto"))
            {
                if (Int32.TryParse(pointsList[0], out form.x1) && Int32.TryParse(pointsList[1], out form.y1))
                {
                    displayMessage("'MoveTo' parameters have been set.");
                    return true;
                }
                else
                {
                    displayMessage("Invalid parameters, try again.");
                    return false;
                }
            }
            else if (text.Equals("drawto"))
            {
                if (Int32.TryParse(pointsList[0], out form.x2) && Int32.TryParse(pointsList[1], out form.y2))
                {
                    form.drawTo();
                }
                else
                {
                    displayMessage("Invalid parameters, try again.");
                    return false;
                }
            }
            return false;
        }

        public bool displayMessage(string message)
        {
            try
            {
                MessageBox.Show(message);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void exit()
        {
            Application.Exit();
        }

        public bool reset(Form1 form)
        {
            try
            {
                form.x1 = 0;
                form.x2 = 0;
                form.y1 = 0;
                form.y2 = 0;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackerApp
{
    public partial class Form1 : Form
    {
        public int x1, y1, x2, y2 = 0;

        public bool useInputPoints = false;

        public Form1()
        {
            InitializeComponent();
        }

        // capture command from CommandBox
        private void CommandBox_KeyUp(object sender, KeyEventArgs e)
        {
            Command command = new Command();

            if (e.KeyCode.Equals(Keys.Enter))
            {
                string commandText = CommandBox.Text.ToLower();
                string[] inputText = InputBox.Lines;
                command.delegateCommand(this, commandText, inputText);
            }
        }

        // Draw a line on the picturebox(name OutputBox) using points entered by user
        public bool drawTo()
        {
            try
            {
                Graphics graphics = OutputBox.CreateGraphics();
                Pen pen = new Pen(Brushes.OrangeRed, 3);
                graphics.DrawLine(pen, this.x1, this.y1, this.x2, this.y2);
                pen.Dispose();
                graphics.Dispose();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        // Draw a line on the picturebox(name OutputBox)
        public bool drawLine()
        {
            try
            {
                Graphics graphics = OutputBox.CreateGraphics();
                Pen pen = new Pen(Brushes.OrangeRed, 3);
                graphics.DrawLine(pen, 
                    this.useInputPoints ? this.x1 : 50,
                    this.useInputPoints ? this.x1 : 50,
                    this.useInputPoints ? this.x1 + 250 : 250,
                    this.useInputPoints ? this.y1 : 50
                );
                pen.Dispose();
                graphics.Dispose();
                this.useInputPoints = false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        // Draw a circle on the picturebox(name OutputBox)
        public bool drawCircle(int radius)
        {
            try
            {
                Graphics graphics = OutputBox.CreateGraphics();
                Pen pen = new Pen(Brushes.DeepSkyBlue, 3);
                graphics.DrawEllipse(pen,
                    this.useInputPoints ? this.x1 : 350,
                    this.useInputPoints ? this.y1 : 50,
                    !radius.Equals(0) ? radius : 150,
                    !radius.Equals(0) ? radius : 150
                );
                pen.Dispose();
                graphics.Dispose();
                this.useInputPoints = false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        // Draw a rectangle on the picturebox(name OutputBox)
        public bool drawRectangle(int width, int hieght)
        {
            try
            {
                Graphics graphics = OutputBox.CreateGraphics();
                Pen pen = new Pen(Brushes.DarkOliveGreen, 3);
                graphics.DrawRectangle(pen,
                    this.useInputPoints ? this.x1 : 50,
                    this.useInputPoints ? this.x1 : 100,
                    !width.Equals(0) ? width : 200,
                    !hieght.Equals(0) ? hieght : 100
                );
                pen.Dispose();
                graphics.Dispose();
                this.useInputPoints = false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        // Draw a triangle on the picturebox(name OutputBox)
        public bool drawTriangle(int bse, int adj, int hyp)
        {
            try
            {
                Graphics graphics = OutputBox.CreateGraphics();
                Pen pen = new Pen(Brushes.DeepSkyBlue, 3);
                graphics.DrawLine(pen,
                    50,
                    450,
                    !bse.Equals(0) ? bse : 250,
                    450
                );
                graphics.DrawLine(pen,
                    !bse.Equals(0) ? bse : 250,
                    450,
                    !hyp.Equals(0) ? bse - hyp : 50,
                    450 / 2
                );
                graphics.DrawLine(pen,
                    !hyp.Equals(0) ? bse - hyp : 50,
                    450 / 2,
                    50,
                    450
                );
                pen.Dispose();
                graphics.Dispose();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        // Clear the picturebox
        public bool clear()
        {
            try
            {
                OutputBox.Refresh();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void loadProgramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Create an openFileDialog to browse computer for desired file.
            OpenFileDialog openFile = new OpenFileDialog();

            // Declare file type to look for
            openFile.DefaultExt = "*.text";
            openFile.Filter = "Text Files|*.txt";

            // Determine if the user selected a file from the saveFileDialog.
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                // Load the contents of the file into the InputBox.
                InputBox.LoadFile(openFile.FileName, RichTextBoxStreamType.PlainText);
                MessageBox.Show("Program loaded succesfully!");
            }
        }

        private void saveProgramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Create a SaveFileDialog to request a path and file name to save to.
            SaveFileDialog saveFile = new SaveFileDialog();

            // Initialize the SaveFileDialog to specify the RTF extension for the file.
            saveFile.DefaultExt = "*.text";
            saveFile.Filter = "Text Files|*.txt";

            // Determine if the user selected a file name from the saveFileDialog.
            if (saveFile.ShowDialog() == DialogResult.OK && saveFile.FileName.Length > 0)
            {
                // Save the contents of the InputBox into the file.
                InputBox.SaveFile(saveFile.FileName, RichTextBoxStreamType.PlainText);
                MessageBox.Show("Program saved succesfully!");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackerApp
{
    public class Code : Form1
    {
        private int loopStart, loopEnd, methodStart, methodEnd, methodCall, ifStart, ifEnd = 0;
        private bool skipLine = false;
        private string[] nullInput = new string[] { };
        Command command = new Command();
        public IDictionary<string, int> variables = new Dictionary<string, int>();

        /// <summary>
        ///     Analise and sort incoming program from file.   
        ///</summary>
        public void parseProgram(Form1 form, string[] inputText)
        {
            for (int i = 0; i < inputText.Length; i++)
            {
                if (!inputText[i].Trim().Equals(""))
                {
                    int lineNumber = i + 1;
                    string currentInputText = inputText[i].Trim().ToLower();
                    string[] splitLine = inputText[i].Trim().ToLower().Split(' ');

                    if (skipLine)
                    {
                        if (splitLine[0] == "endloop")
                        {
                            loopEnd = i;
                            loopProgram(form, inputText);
                            skipLine = false;
                        }
                        if (splitLine[0] == "endmethod")
                        {
                            methodEnd = i;
                            skipLine = false;
                        }
                        if (splitLine[0] == "endif")
                        {
                            ifEnd = i;
                            ifProgram(form, inputText);
                            skipLine = false;
                        }
                        else
                        {
                            continue;
                        }
                    } 
                    else
                    {
                        if (splitLine[0] == "loop")
                        {
                            loopStart = i;
                            skipLine = true;
                        }
                        if (splitLine[0] == "method")
                        {
                            methodStart = i;
                            skipLine = true;
                        }
                        if (splitLine[0] == "if")
                        {
                            ifStart = i;
                            skipLine = true;
                        }
                        else
                        {
                            delegateCommand(form, splitLine, currentInputText, lineNumber, inputText);
                        }
                    }
                }
            }
        }

        /// <summary>
        ///        Deal with keyword commands such as reset and clear
        ///</summary>
        public void delegateCommand(Form1 form, string[] splitLine, string currentInputText, int lineNumber, string[] inputText)
        {
            if (variableExists(splitLine[0]) && splitLine.Length.Equals(3) && Int32.TryParse(splitLine[2], out int value))
            {
                editVariable(splitLine, value, lineNumber);
            }
            else
            {
                if (splitLine[0] == "var")
                {
                    addVariable(lineNumber, splitLine);
                }
                else if (splitLine[0] == "circle")
                {
                    circleProgram(form, splitLine, currentInputText, lineNumber);
                }
                else if (splitLine[0] == "rectangle")
                {
                    command.delegateCommand(form, currentInputText, nullInput);
                }
                else if (splitLine[0] == "triangle")
                {
                    command.delegateCommand(form, currentInputText, nullInput);
                }
                else if (splitLine[0] == "line")
                {
                    command.delegateCommand(form, currentInputText, nullInput);
                }
                else if (splitLine[0] == "moveto")
                {
                    command.delegateCommand(form, currentInputText, nullInput);
                }
                else if (splitLine[0] == "drawto")
                {
                    command.delegateCommand(form, currentInputText, nullInput);
                }
                else if (splitLine[0] == "reset")
                {
                    command.delegateCommand(form, currentInputText, nullInput);
                }
                else if (splitLine[0] == "clear")
                {
                    command.delegateCommand(form, currentInputText, nullInput);
                }
                else if (splitLine[0] == "call")
                {
                    methodCall = lineNumber - 1;
                    methodProgram(form, inputText);
                }
            }
        }

        /// <summary>
        ///        Handles if commands from program.
        ///</summary>
        public bool ifProgram(Form1 form, string[] inputText)
        {
            string[] ifCommand = inputText[ifStart].Trim().ToLower().Split(' ');
            if (ifCommand.Length.Equals(4) && variableExists(ifCommand[1]) && ifCommand[2].Equals("=") && Int32.TryParse(ifCommand[3], out int param))
            {
                if (getVariableAsInt(ifCommand[1]) == param)
                {
                    for (int i = ifStart + 1; i < ifEnd; i++)
                    {
                        int lineNumber = i + 1;
                        string currentInputText = inputText[i].Trim().ToLower();
                        string[] splitLine = inputText[i].Trim().ToLower().Split(' ');

                        delegateCommand(form, splitLine, currentInputText, lineNumber, inputText);
                    }
                }
                return true;
            }
            else
            {
                int lineNumber = ifStart + 1;
                displayMessage("Syntax error on line " + lineNumber + " while declaring if statement.");
                return false;
            }
        }

        /// <summary>
        ///        Handles method commands from program.
        ///</summary>
        public bool methodProgram(Form1 form, string[] inputText)
        {
            string[] methodCommand = inputText[methodStart].Trim().ToLower().Split(' ');
            string[] methodCallCommand = inputText[methodCall].Trim().ToLower().Split(' ');
            int callLength = methodCallCommand.Length;
            int commandLength = methodCommand.Length;

            if (commandLength.Equals(2) && callLength.Equals(2) && methodCommand[1].Equals(methodCallCommand[1]))
            {
                for (int i = methodStart + 1; i < methodEnd; i++)
                {
                    int lineNumber = i + 1;
                    string currentInputText = inputText[i].Trim().ToLower();
                    string[] splitLine = inputText[i].Trim().ToLower().Split(' ');

                    delegateCommand(form, splitLine, currentInputText, lineNumber, inputText);
                }
                return true;
            }
            else if (commandLength.Equals(3) && callLength.Equals(3) && methodCommand[1].Equals(methodCallCommand[1]))
            {
                string param = methodCallCommand[2];
                if (param.StartsWith("(") && param.EndsWith(")") && param.Length > 2 && Int32.TryParse(param.Substring(1, param.Length - 2), out int paramValue))
                {
                    string methodParam = methodCommand[2];
                    if (methodParam.StartsWith("(") && methodParam.EndsWith(")") && methodParam.Length > 2)
                    {
                        string paramKey = methodParam.Substring(1, methodParam.Length - 2);
                        variables.Add(paramKey, paramValue);

                        if (variableExists(paramKey))
                        {
                            for (int i = methodStart + 1; i < methodEnd; i++)
                            {
                                int lineNumber = i + 1;
                                string currentInputText = inputText[i].Trim().ToLower();
                                string[] splitLine = inputText[i].Trim().ToLower().Split(' ');

                                delegateCommand(form, splitLine, currentInputText, lineNumber, inputText);
                            }
                        }
                    }
                    else
                    {
                        int lineNumber = methodCall;
                        displayMessage("Syntax error with method declaration on line " + lineNumber + ", try again.");
                        return false;
                    }
                    return true;
                }
                else
                {
                    int lineNumber = methodCall;
                    displayMessage("Syntax error with method call on line " + lineNumber + ", try again.");
                    return false;
                }
            }
            else
            {
                int lineNumber = methodCall;
                displayMessage("Call to an undefined method on line " + lineNumber + ", try again.");
                return false;
            }
        }

        /// <summary>
        ///        Handles loop commands from program.
        ///</summary>
        public bool loopProgram(Form1 form, string[] inputText)
        {
            string[] loopCommand = inputText[loopStart].Trim().ToLower().Split(' ');

            if (loopCommand.Length.Equals(3) && loopCommand[1].Equals("for"))
            {
                if (variableExists(loopCommand[2]))
                {
                    for (int x = 0; x < getVariableAsInt(loopCommand[2]); x++)
                    {
                        for (int i = loopStart + 1; i < loopEnd; i++)
                        {
                            int lineNumber = i + 1;
                            string currentInputText = inputText[i];
                            string[] splitLine = inputText[i].Trim().ToLower().Split(' ');

                            delegateCommand(form, splitLine, currentInputText, lineNumber, inputText);
                        }
                    }
                    return true;
                }
                else
                {
                    int lineNumber = loopStart + 1;
                    displayMessage("The variable '" + loopCommand[2] + "' on line " + lineNumber + " has not been initialised.");
                    return false;
                }
            }
            else
            {
                int lineNumber = loopStart + 1;
                displayMessage("Syntax error on line " + lineNumber + ", try again.");
                return false;
            }
        }

        /// <summary>
        ///        Handles variable creation.
        ///</summary>
        public bool addVariable(int lineNumber, string[] splitLine)
        {
            if (splitLine.Length.Equals(4))
            {
                try
                {
                    if (variableExists(splitLine[1]))
                    {
                        variables.Remove(splitLine[1]);
                        variables.Add(splitLine[1], Int32.Parse(splitLine[3]));
                        return true;
                    } 
                    else
                    {
                        variables.Add(splitLine[1], Int32.Parse(splitLine[3]));
                        return true;
                    }
                }
                catch (Exception)
                {
                    displayMessage("Syntax Error on line " + lineNumber + " : problem with variable declaration.");
                    return false;
                }
            }
            else
            {
                displayMessage("Syntax Error on line " + lineNumber + " : problem with variable declaration.");
                return false;
            }
        }

        /// <summary>
        ///        Edits variables within program.
        ///</summary>
        public bool editVariable(string[] splitLine, int value, int lineNumber)
        {
            if (splitLine[1] == "+")
            {
                value = getVariableAsInt(splitLine[0]) + value;
                variables.Remove(splitLine[0]);
                variables.Add(splitLine[0], value);
                return true;
            }
            else if (splitLine[1] == "-")
            {
                value = getVariableAsInt(splitLine[0]) - value;
                variables.Remove(splitLine[0]);
                variables.Add(splitLine[0], value);
                return true;
            }
            else
            {
                displayMessage("Syntax error on line " + lineNumber + ", operator has to be '+' or '-'.");
            }
            return false;
        }

        /// <summary>
        ///        Ensures a given variable is already declared.
        ///</summary>
        public bool variableExists(string key)
        {
            if (variables.ContainsKey(key))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///        Returns variable values as strings.
        ///</summary>
        public string getVariable(string key)
        {
            return variables[key].ToString();
        }

        /// <summary>
        ///        Returns variable values as integers.
        ///</summary>
        public int getVariableAsInt(string key)
        {
            return variables[key];
        }

        /// <summary>
        ///        Handles circle commands from program.
        ///</summary>
        public bool circleProgram(Form1 form, string[] splitLine, string inputText, int lineNumber)
        {
            if (splitLine.Length.Equals(2))
            {
                if (variableExists(splitLine[1]))
                {
                    string key = splitLine[1];
                    string commandText = splitLine[0] + " " + getVariable(key);
                    command.delegateCommand(form, commandText, nullInput);
                }
                else
                {
                    command.delegateCommand(form, inputText, nullInput);
                }
                return true;
            }
            else
            {
                displayMessage("Invalid circle command on line " + lineNumber + ", try again.");
            }
            return false;
        }


        /// <summary>
        ///        Displays messages using message box.
        ///</summary>
        public bool displayMessage(string message)
        {
            try
            {
                MessageBox.Show(message);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
